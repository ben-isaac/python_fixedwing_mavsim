"""
mavsim_python: mav viewer (for chapter 2)
    - Beard & McLain, PUP, 2012
    - Update history:
        2/24/2020 - RWB
"""
import sys
sys.path.append("..")
import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl
import pyqtgraph.Vector as Vector
import matplotlib.pyplot as plt
from tools.drawing import drawMav


class mavViewer():
    def __init__(self):
        # initialize Qt gui application and window
        self.app = pg.QtGui.QApplication([])  # initialize QT
        self.window = gl.GLViewWidget()  # initialize the view object
        self.window.setWindowTitle('MAV Viewer')
        self.window.setGeometry(100, 100, 1000, 1000)  # args: upper_left_x, upper_right_y, width, height
        
        grid = gl.GLGridItem()  # make a grid to represent the ground
        grid.scale(10, 10, 10)  # set the size of the grid (distance between each line)
        
        self.window.addItem(grid)  # add grid to viewer
        y = np.linspace(-1000, 1000, 10)
        x = np.linspace(-1000,1000, 10)
        temp_z = np.random.rand(len(x),len(y))*100.0 -100
        cmap = plt.get_cmap('terrain') # Try 'pink' 'copper' or 'terrain'
        minZ=np.min(temp_z)
        maxZ=np.max(temp_z)
        rgba_img = cmap((temp_z-minZ)/(maxZ -minZ))
        surf = gl.GLSurfacePlotItem(x=y, y=x, z=temp_z, colors = rgba_img )
        surf.scale(3, 1, 1)
        self.window.addItem(surf)
        
        self.window.setCameraPosition(distance=200) # distance from center of plot to camera
        self.window.setBackgroundColor(0, 100, 200)  # set background color to black
        self.window.show()  # display configured window
        self.window.raise_()  # bring window to the front
        self.plot_initialized = False  # has the mav been plotted yet?
        self.mav_plot = []

    def update(self, state):
        # initialize the drawing the first time update() is called
        if not self.plot_initialized:
            self.mav_plot = drawMav(state, self.window)
            self.plot_initialized = True
        # else update drawing on all other calls to update()
        else:
            self.mav_plot.update(state)
        # update the center of the camera view to the mav location
        view_location = Vector(state.pe, state.pn, state.h)  # defined in ENU coordinates
        self.window.opts['center'] = view_location
        # redraw
        self.app.processEvents()
